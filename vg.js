//Først fjerner vi teaser
kvasbo.j("#plussTeaser").hide();

//Gjem alle lenker
kvasbo.j('a[href*="pluss.vg.no"]').hide();

//Gjem alle artikler som inneholder lenke til pluss
kvasbo.j('div[class*="article-content"]').each(function(){

	var num = kvasbo.j(this).find('a[href*="pluss.vg.no"]').size()
		
	if(num > 0)
	{
		kvasbo.j(this).hide();
	}
});